#!/bin/bash

sudo apt-get update -y
sudo apt-get upgrade -y
sudo apt-get install docker.io && tesseract-ocr && libtesseract-dev -y
curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod 777 /usr/local/bin/docker-compose
sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose