# Welcome my project

## env
- Ubuntu 18.04
- python 3.8.6

# Step
## 環境設置
首先執行 project 下的 startup.sh
```
. startup.sh
```
---
### 試題1
1. 進入試題1資料夾
```
cd <project>/試題1
```
2. 執行 BASH 檔案
```
. startup.sh
```
3. 確認程式執行  
crawler : 主要執行爬蟲程式檔案  
database : MongoDB Container
```
docker ps
```

>> Notice!!  
>> 該程式尚未加入去重判定，因此重複執行會使資料汙染
---
### 試題2
1. 進入試題2資料夾
```
cd <project>/試題2
```
2. 執行 BASH 檔案
```
. startup.sh
```
3. 確認程式執行  
build_api : 主要提供對應資料API
- /getMaxMinlicense  
取得最多以及最少發照數量之市區
>> 樹林區擁有最多發照數量:4504, 新莊區擁有最少數量:1
- /findLogestLicense
取得最長發照期間的建築物市區
>> 樹林區
- /findLogestLicenseDesignOther
確認最久發照日期之設計人or起照人是否有其他新北市建築物
>> 該人在新北市有其他建築物
- /findMostDangerCity
尋找危險率最高的市區
>> 三重區
--- 
### 試題3
未完，待續
