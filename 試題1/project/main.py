from loguru import logger
import requests
from lxml import etree
from datetime import datetime
import codecs
import os
from time import sleep
import re
import threading
from read_captcha import read_captcha_word
from connection import Connection
from postal_code import PostalCodeCrawler


postalcrawler = PostalCodeCrawler()
class BuildingManage:
    def __init__(self):
        self.requests = requests.Session()
        self.requests.headers = self.__set_headers()
        self.postal_dict = postalcrawler.get_postal_code()
        self.connection = Connection()
        self.task_list = []
        self.lock = threading.Lock()
        
    def __get_cookies_headers(self):
        headers = {
            'Host': 'building-management.publicwork.ntpc.gov.tw',
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:102.0) Gecko/20100101 Firefox/102.0',
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8',
            'Accept-Language': 'zh-TW,zh;q=0.8,en-US;q=0.5,en;q=0.3',
            'Accept-Encoding': 'gzip, deflate',
        }
        return headers
    
    def __set_headers(self):
        headers_cookie = ''
        headers = self.__get_cookies_headers()
        cookies = self.get_cookies()
        for k, v in cookies.items():
            headers_cookie += f'{k}={v}; '
        headers['Cookie'] = headers_cookie[:-2] # 去除結尾; 
        return headers
        
    def get_cookies(self):
        self.requests.get('https://building-management.publicwork.ntpc.gov.tw/bm_list.jsp',
                        headers = self.__get_cookies_headers(),
                        allow_redirects=True)
        return self.requests.cookies
        
    def get_captcha(self):
        now = datetime.timestamp(datetime.now())
        params = {'time':now}
        response = self.requests.get('https://building-management.publicwork.ntpc.gov.tw/ImageServlet',
                                     params = params)
        with codecs.open('captcha.jpg', 'wb')as f:
            f.write(response.content)
    
    def get_captcha_word(self):
        word = read_captcha_word('captcha.jpg')
        os.remove('captcha.jpg') # 用完就丟
        return word
    
    def check_captcha_ok(self, code):
        data = {'code':code}
        response = self.requests.post('https://building-management.publicwork.ntpc.gov.tw/CheckCode', 
                                 json = data)
        msg = response.text
        if msg == 'true':
            return True
        return False
        
    def get_build_manage_data(self, code, postal_code, page):
        data = {
            'rt':'BM', # 執照基本資料
            'PagePT':page,
            'A2':'3', # 發證別(3.使用執照)
            # 'D1V':'%B7s%A5_%A5%AB%AAO%BE%F4%B0%CF', # 市區名
            'D1':postal_code, # 郵遞區號
            # 'D3':'%A4T%A5%C1%B8%F4', # 路段名
            'Z1':code, # 驗證碼
        }
        response = self.requests.get('https://building-management.publicwork.ntpc.gov.tw/bm_list.jsp',
                                        params = data)
        return response
    
    def parse_get_total_page(self, response):
        html = etree.HTML(response.text)
        total_page = html.xpath('//tfoot[@id="DataButton"]//b/text()')
        split_last_page = total_page[0].split('，')[-1]
        last_page = re.findall(r'([0-9][1-9]*)', split_last_page) # [' [ 第1頁，共73頁 ] ']
        if last_page:
            return int(last_page[0])
        return 0
        
    def parse_build_manage_data_page(self, postal_code, response):
        html = etree.HTML(response.text)
        data_list = html.xpath('//tbody[@id="DataBlock"]/tr')
        data_key = html.xpath('//tr/th//text()')
        for _data in data_list:
            # 暫不明確資料格式，先做try catch
            try:
                data = _data.xpath('.//td//text()')
                data_dict = {k:v for k, v in zip(data_key, data)}
                data_dict['region'] = self.postal_dict[postal_code]
                self.lock.acquire()
                self.connection.insert_data(data_dict)
                self.lock.release()
                logger.debug(f'{data_dict}資料匯入成功{datetime.now()}')
            except:
                pass
    
    def crawler_and_save(self, code, postal_code, page):
        response = self.get_build_manage_data(code, postal_code, page)
        self.parse_build_manage_data_page(postal_code, response)
    
    def main(self):
        postalcode_list = self.postal_dict.keys()
        # code = input(str('code:'))
        # 若失敗則重跑
        # 暫時移除，不影響程式操作
        # if not self.check_captcha_ok(code):
            # self.main()
        for postal_code in postalcode_list:
            page = 1
            # 先預設每換一次地點就得輸入一次不同驗證碼，驗證失敗重新驗證
            while True:
                try:
                    self.get_captcha()
                    code = self.get_captcha_word()
                    
                    last_page = 1 # 先預設1
                    response = self.get_build_manage_data(code, postal_code, page)
                    last_page = self.parse_get_total_page(response)
                    break
                except Exception as check_err:
                    logger.info(check_err)
            
            while page <= last_page:
                print(page)
                task = threading.Thread(target=self.crawler_and_save, args=(code, postal_code, page,))
                task.start()
                self.task_list.append(task)
                sleep(0.5)
                page += 1
            for task in self.task_list:
                task.join()
                    
        self.connection.close_connection()
        
    
if __name__ == '__main__':
    buildingManage = BuildingManage()
    buildingManage.main()