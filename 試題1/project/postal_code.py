import requests
from lxml import etree


class PostalCodeCrawler:
    def __init__(self):
        self.requests = requests.Session()
        self.requests.headers = self.__set_headers()
        self.task_list = []
        
    def __set_headers(self):
        headers = {
            'Host': 'zip5.5432.tw',
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:102.0) Gecko/20100101 Firefox/102.0',
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8',
            'Accept-Language': 'zh-TW,zh;q=0.8,en-US;q=0.5,en;q=0.3',
            'Accept-Encoding': 'gzip, deflate',
        }
        return headers
    
    def get_postal_code(self):
        """取得新北市所有郵遞區號\n
        ex:{241:'三重市'}
        """
        response = self.requests.post(f'https://zip5.5432.tw/cityzip/新北市')
        response.encoding = 'utf-8'
        html = etree.HTML(response.text)
        postal_list = html.xpath('//table//td[@class="zip-zip"]//text()')
        region_list = html.xpath('//table//td[@class="zip-area"]//text()')
        location_dict = {postal:region for postal, region in zip(postal_list, region_list)}
        return location_dict
        
            
if __name__ == '__main__':
    postalcrawler = PostalCodeCrawler()
    postal_list = postalcrawler.get_postal_code()
    print(postal_list)
    
        