import pandas as pd
from connection import Connection


conn = Connection()
class Aggregate:
    def get_all_data(self):
        data_list = conn.list_data()
        return data_list
    
    def get_all_dataframe(self):
        data_list = self.get_all_data()
        df = pd.DataFrame(data_list)
        df.fillna('', inplace=True)
        return df
    
    def download_data(self):
        df = self.get_all_dataframe()
        df.to_csv('新北市使用執照資訊.csv')
    
    def get_license_count(self):
        region_count_dict = {}
        df = self.get_all_dataframe()        
        df = df[df['發照日期'] != '']

        df_group = df.groupby(['region'])
        for region, group in df_group.groups.items():
            count = len(group)
            region_count_dict[region] = count
        
        data_sort = sorted(region_count_dict.items(), key=lambda x:x[1])
        min = data_sort[0]
        max = data_sort[-1]
        
        min_city = min[0]
        min_count = min[1]
        max_city = max[0]
        max_count = max[1]
        output = {'message':f'{max_city}擁有最多發照數量:{max_count}, {min_city}擁有最少數量:{min_count}'}
        return output
    
    def find_logest_license(self):
        df = self.get_all_dataframe()
        drop_no_license_df = df[df['發照日期'] != '']      
        drop_no_license_df.sort_values(['發照日期'], ascending=[True], inplace=True)
        longest_dict = drop_no_license_df.iloc[0]
        return longest_dict
    
    def find_logest_license_region(self):
        longest_dict = self.find_logest_license()
        return longest_dict['region']
    
    def find_logest_license_design_other(self):
        have_other = False
        msg = '沒有'
        df = self.get_all_dataframe()
        longest_dict = self.find_logest_license()
        match_create_license_person_df = df[df['起造人'] == longest_dict['起造人']]
        match_designer_df = df[df['設計人'] == longest_dict['設計人']]
        
        if len(match_create_license_person_df):
            have_other = True
        if len(match_designer_df):
            have_other = True
        
        if have_other:
            msg = '有'
        output = {'message':f'該建築物起照者或設計人在新北市 "{msg}" 其他建築物'}
        return output
        
    def find_most_danger_city(self):
        region_count_dict = {}
        df = self.get_all_dataframe()
        df = df[df['發照日期'] > '109/11/1']
        df_group = df.groupby(['region'])
        for region, group in df_group.groups.items():
            count = len(group)
            region_count_dict[region] = count
        data_sort = sorted(region_count_dict.items(), key=lambda x:x[1])
        most_region_dict = data_sort[-1]
        most_region = most_region_dict[0]
        output = {'message':f'{most_region}擁有最多的發照期間建築，因此該區的電線走火機率較高'}
        return output
        
        
if __name__ == '__main__':
    agg = Aggregate()
    # print(agg.get_license_count())
    # print(agg.find_logest_license_region())
    # print(agg.find_logest_license_design_other())
    # print(agg.find_most_danger_city())
    print(agg.download_data())
