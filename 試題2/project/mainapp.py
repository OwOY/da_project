from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from fastapi.responses import FileResponse
import uvicorn
from service import Aggregate

app = FastAPI()
agg = Aggregate()

app.add_middleware(
    CORSMiddleware, 
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.get('/getMaxMinlicense')
def get_max_min_license():
    """取得最多以及最少發照數量市區

    Returns:
        json: 內容描述
    """
    return agg.get_license_count()

@app.get('/getexcel')
def getExcel():
    """取得所有資料的Excel(hdf)

    Returns:
        file: 下載的Excel
    """
    agg.download_data()
    return FileResponse('新北市使用執照資訊.csv', filename='新北市使用執照資訊.csv', media_type='hdf')

@app.get('/findLogestLicenseRegion')
def find_logest_license_region():
    """取得最久發照日期區域

    Returns:
        json: 文字描述
    """
    return agg.find_logest_license_region()

@app.get('/findLogestLicenseDesignOther')
def find_logest_license_design_other():
    """確認最久發照日期之設計人or起照人是否有其他新北市建築物

    Returns:
        json: 文字描述
    """
    return agg.find_logest_license_design_other()

@app.get('/findMostDangerCity')
def find_most_danger_city():
    """尋找危險率最高的市區
    
    Returns:
        json: 文字描述
    """
    return agg.find_most_danger_city()


if __name__ == '__main__':
    uvicorn.run('mainapp:app', host='0.0.0.0', port=8888, reload=True)