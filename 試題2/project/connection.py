import pymongo


class Connection:
    def __init__(self):
        self.connection = self.connect()
        
    def connect(self):
        connection = pymongo.MongoClient(host='database',
                                         port=27017,
                                         username='da',
                                         password='PassW0rd',
                                         )
        return connection
    
    # def create_table(self):
    def insert_data(self, data_dict):
        self.connection['project']['build_manage'].insert_one(data_dict)
    
    def list_data(self):
        data_list = self.connection['project']['build_manage'].find({}, {'_id':False})
        output = [data for data in data_list]
        return output
        
    def close_connection(self):
        self.connection.close()
        

if __name__ == '__main__':
    con = Connection()
    con.insert_data({'test':'tewtsts'})
    con.list_data()
    con.close_connection()